
function getCustomerData() {
	WL.Logger.debug("CustomerDataAdapter.getCustomerData procedure invoked");
	var input = {
		    method : 'get',
		    returnedContentType : 'json',
		    path : "/apps/services/www/insuranceJq/mobilewebapp/default/json/Customer.json"
		};
		
		
		return WL.Server.invokeHttp(input);

}