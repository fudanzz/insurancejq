
/* JavaScript content from js/controllers/PolicyController.js in folder common */
(function(){	
	jq("#policyView").live("pageshow", function(e, data){
		WL.Logger.debug("policy view");
		var customer=insurance.model.Customer;	
		var policyData=customer.getPolicyData();
		if(policyData){
			WL.Logger.debug("policy data is ready");	
			handlePolicy(policyData);
		}else{
			WL.Logger.debug("policy data is null");	
			loadCustomerPolicyFromServer();
		}
		
	});
	
	function loadCustomerPolicyFromServer()
	{		
		try
		{		
			var invocationData =
			{
				adapter:"CustomerDataAdapter",
				procedure:"getCustomerData",
				parameters:[]
			};
		
		var options =
			{
				onSuccess:customerLoaded,
				onFailure:customerError
			};
		WL.Logger.debug("About to invoke loadCustomerFromServer");
		WL.Client.invokeProcedure(invocationData, options);
	   }
		catch(e)
		{
			WL.Logger.debug("Failed to load Customer Information: "+e);
		}
	
	}
	
	function customerLoaded(result)
    {
		WL.Logger.debug("policy data: "+ result.invocationResult);
		insurance.model.Customer["policyData"]= result.invocationResult;
		handlePolicy(insurance.model.Customer["policyData"]);
    }
	
    function customerError(error)
    {
    	WL.Logger.debug("Failed to load Customer", error);
    }

	function handlePolicy(data){		
	
	}
	

})();