
/* JavaScript content from js/insuranceJq.js in folder common */

function wlCommonInit(){
	// Common initialization code goes here
	jq.mobile.changePage("views/policyView.html");
}
/* JavaScript content from js/insuranceJq.js in folder iphone */

// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}