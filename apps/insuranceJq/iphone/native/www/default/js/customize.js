
/* JavaScript content from js/customize.js in folder common */
window.jq=jQuery.noConflict();

jq(document).bind("mobileinit", function() {
	jq.extend(jq, {
		namespace: function(namespace){
			var names = namespace.split(".");
			var parent = window;
			for (var i = 0; i < names.length; ++i){
				var name = names[i];
				if (typeof parent[name] === "undefined") {
					parent[name] = {};
				}
				parent = parent[name];
			}
			return parent;
		},
		extendModule: function(name, publicInterface){
			var module = this.namespace(name);
			this.extend(module, publicInterface);
			return module;
		}
	});
	
	jq.extend(jq.mobile, {
		// forbid page transition animation
		defaultPageTransition : "none",
		pushStateEnabled: false //android and ios bug on HTML5 pushState, see: https://github.com/jquery/jquery-mobile/issues/3939
	});
});