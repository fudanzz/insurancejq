(function() {
	//all the data model are defined in this js file by extending module
	
	jq.extendModule("insurance.model.Customer", {
		policyData:null,
		claimData:null,
		billingData:null,
		getPolicyData:function(){
			return this.policyData;
		},
		getBillingData:function(){
			return this.billingData;
		},
		getClaimData:function(){
			return this.claimData;
		},
	});
	
	jq.extendModule("insurance.model.XXXX", {
		xxxx:null,
	});
	
})();